import { db } from "./db";

export const getMeals = async () => {
    const connection = await db.getConnection();
    return await connection.collection('meals')
        .find({})
        .toArray();
}
