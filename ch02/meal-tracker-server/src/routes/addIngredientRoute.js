import { getIngredients, insertIngredient } from "../db";

export const addIngredientRoute = {
    method: 'post',
    path: '/ingredients',
    handler: async (req, res) => {
        const ingredient = req.body;
        await insertIngredient(ingredient);
        const updateIngredients = await getIngredients();
        res.status(200).json(updateIngredients);
    },
};
