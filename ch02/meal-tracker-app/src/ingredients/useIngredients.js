import { useEffect, useState } from "react";

export const useIngredients = () => {
    const [ isLoading, setIsLoading ] = useState(true);
    const [ ingredients, setIngredients ] = useState([]);

    useEffect(() => {
        const loadingIngredients = async () => {
            const response = await fetch('/ingredients');
            const ingredientsResponse = await response.json();
            setIngredients(ingredientsResponse);
            setIsLoading(false);
        };

        loadingIngredients();
    }, []);

    return { isLoading, ingredients, setIngredients };
}
