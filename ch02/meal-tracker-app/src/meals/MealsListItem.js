import { SmallX } from "../ui";
import { Link } from "react-router-dom";

export const MealsListItem = ({ meal, date, onDelete }) => {
    return (
        <div className="list-item">
            {meal ? (
                <>
                    <h3>{date.getDate()}</h3>
                    <p>{meal.recipe.name}</p>
                    <div className="right-action">
                        <SmallX onClick={() => onDelete(meal._id)} />
                    </div>
                </>
            ) : (
                <>
                    <h3>{date.getDate()}</h3>
                    <>No meal planned</>
                    <div className="right-action">
                        <Link to={`/recipes?date=${date.toISOString()}`}>
                            <button>Add</button>
                        </Link>
                    </div>
                </>
            )}
        </div>
    );
};
