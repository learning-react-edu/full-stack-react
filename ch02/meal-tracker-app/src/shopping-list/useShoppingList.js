import { useEffect, useState } from "react";

export const useShoppingList = () => {
    const [ isLoading, setIsLoading ] = useState(true);
    const [items, setItems] = useState([]);

    useEffect(() => {
        const loadingShoppingList = async () => {
            const response = await fetch('/shopping-list');
            const items = await response.json();
            setItems(items);
            setIsLoading(false);
        };
        loadingShoppingList();
    }, []);

    return { isLoading, items };
};
