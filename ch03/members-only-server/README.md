# Members only website server

## Generate fake data JSON files
To generate fake data JSON files enter real user IDs from firebase to the `fake-data.js` file.
Create `temp` directory in the root of the project.
Then execute following command in the root of the project:
```shell
node src/conert-to-json.js
```

This script will create 4 JSON files into `temp` directory.

## Import fake data into MongoDB

Import data from those files into mongo database with the following commands executed in `temp` directory:
```shell
mongoimport --db="members-only" --collection="groups" --file="./groups.json" --jsonArray
mongoimport --db="members-only" --collection="messages" --file="./messages.json" --jsonArray
mongoimport --db="members-only" --collection="requests" --file="./requests.json" --jsonArray
mongoimport --db="members-only" --collection="users" --file="./users.json" --jsonArray
```

### Using docker container
If MongoDB is running in docker container, then `mongoimport` should be run in the docker container shell.
First those files must be copied to the container with the following command:
```shell
docker cp "./temp" full_stack_react_mongo:"/data"
```

Here `full_stack_react_mongo` is the name of the container.

Then open docker CLI in the selected container, move to the JSON file location (`/data/temp`) 
and execute same `mongoimport` command listed above.
