import { getGroup } from "./getGroup";
import { getMessagesForGroup } from "./getMessagesForGroup";

export const getMemberPopulatedGroup = async (groupId) => {
    const group = await getGroup(groupId);
    const messages = await getMessagesForGroup(groupId);
    return {
        ...group,
        messages,
    };
};
