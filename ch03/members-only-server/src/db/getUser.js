import { db } from "./db";

export const getUser = async (id) => {
    const connection = db.getConnection();
    return await connection.collection('users')
        .findOne({ id });
}
