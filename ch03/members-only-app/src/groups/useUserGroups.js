import { useEffect, useState } from "react";
import firebase from "firebase/app";

export const useUserGroups = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [userGroups, setUserGroups] = useState([]);

    useEffect(() => {
        const loadGroups = async () => {
            const user = firebase.auth().currentUser;
            if (!user) {
                setUserGroups([]);
                setIsLoading(false);
                return;
            }
            const response = await fetch(`/users/${user.uid}/groups`, {
                headers: {
                    AuthToken: await user.getIdToken(),
                },
            });
            const groupsResponse = await response.json();
            setUserGroups(groupsResponse);
            setIsLoading(false);
        }

        loadGroups();
    }, []);

    return { isLoading, userGroups };
}
