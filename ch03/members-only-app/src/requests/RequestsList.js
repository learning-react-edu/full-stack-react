import { RequestsListItem } from "./RequestsListItem";

export const RequestsList = ({ requests, onAccept, onReject }) => (
    <>
        <h2 className="section-heading">Join Requests</h2>
        {requests.length > 0
            ? (requests.map(request => (
                <RequestsListItem
                    key={request._id}
                    onAccept={onAccept}
                    onReject={onReject}
                    request={request}/>)))
            : (<p>No pending requests</p>)
        }
    </>
);
