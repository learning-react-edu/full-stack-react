import firebase from "firebase/app";
import { useEffect, useState } from "react";

export const useUser = () => {
    const [userInfo, setUserInfo] = useState(() => {
        const user = firebase.auth().currentUser;
        const isLoading = !user;
        return { isLoading, user };
    });

    useEffect(() => {
        return firebase.auth().onAuthStateChanged(user => {
            setUserInfo({ isLoading: false, user });
        });
    }, []);

    return userInfo;
};
