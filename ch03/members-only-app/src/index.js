import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCTXs_4zl540OGCi-cUEJ4aPWC-3C2B63I",
    authDomain: "members-only-bc9c2.firebaseapp.com",
    projectId: "members-only-bc9c2",
    storageBucket: "members-only-bc9c2.appspot.com",
    messagingSenderId: "473984822301",
    appId: "1:473984822301:web:f3798be97bb498f1e7aaf4"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
