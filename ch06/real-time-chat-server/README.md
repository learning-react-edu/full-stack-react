# Real-Time Chat server

## Generate fake data JSON files
To generate fake data JSON files enter real user IDs from firebase to the `fake-data.js` file.
Create `real-time-chat` directory in the root of the project.
Then execute following command in the root of the project:
```shell
node src/convert-to-json.js
```

This script will create single JSON file into `real-time-chat` directory.

## Import fake data into MongoDB

Import data from those files into mongo database with the following commands executed in `real-time-chat` directory:
```shell
mongoimport --db="real-time-chat" --collection="users" --file="./users.json" --jsonArray
```

### Using docker container
If MongoDB is running in docker container, then `mongoimport` should be run in the docker container shell.
First those files must be copied to the container with the following command:
```shell
docker cp "./real-time-chat" full_stack_react_mongo:"/data"
```

Here `full_stack_react_mongo` is the name of the container.

Then open docker CLI in the selected container, move to the JSON file location (`/data/real-time-chat`) 
and execute same `mongoimport` command listed above.
