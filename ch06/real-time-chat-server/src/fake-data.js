const USER_1_ID = 'mIN2254fiERcZJ84a1C3v7JmFOn2';
const USER_2_ID = 'rDzOWLJJbQbWpAEL4UYBxFecP9K2';
const USER_3_ID = 'JzvsTEKEGZRHwnwt4f7TPz8Fdgz1';

module.exports.users = [{
    id: USER_1_ID,
    name: 'Jim Smith',
}, {
    id: USER_2_ID,
    name: 'Jane Jones',
}, {
    id: USER_3_ID,
    name: 'Brenda Brown',
}];
