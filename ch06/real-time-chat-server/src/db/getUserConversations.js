import { db } from "./db";

export const getUserConversations = async (userId) => {
    return await db.getConnection().collection('conversations')
        .find({ memberIds: userId })
        .toArray();
};
