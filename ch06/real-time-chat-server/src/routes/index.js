import { getUserConversationsRoute } from "./getUserConversationsRoute";
import { getAllUsersRoute } from "./getAllUsersRoute";
import { createConversationRoute } from "./createConversationRoute";

export { protectRoute } from './protectRoute';
export const routes = [
    createConversationRoute,
    getAllUsersRoute,
    getUserConversationsRoute,
];
