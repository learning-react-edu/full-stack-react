import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import * as admin from 'firebase-admin';
import socketIo from 'socket.io';
import { addMessageToConversation, db, getCanUserAccessConversation, getConversation } from './db';
import { protectRoute, routes } from './routes';
import credentials from './credentials.json';

admin.initializeApp({
    credential: admin.credential.cert(credentials)
});

const app = express();
app.use(bodyParser.json());

routes.forEach(route => {
    app[route.method](route.path, protectRoute, route.handler);
});

const server = http.createServer(app);
const io = socketIo(server, {
    cors: {
        origin: '*',
        methods: [ 'GET', 'POST' ],
    }
});

io.use(async (socket, next) => {
    console.log('Verifying user auth token...');
    if (!socket.handshake.query || !socket.handshake.query.token) {
        socket.emit('error', 'You need to include an auth token');
    } else {
        socket.user = await admin.auth().verifyIdToken(
            socket.handshake.query.token
        );
    }

    next();
});

io.on('connection', async socket => {
    console.log('A new client has connected to socket.io!');
    const { conversationId } = socket.handshake.query;
    const conversation = await getConversation(conversationId);
    socket.emit('heresYourConversation', conversation);

    socket.on('postMessage', async ({ text, conversationId }) => {
        const { user_id: userId } = socket.user;
        const userIsAuthorized = await getCanUserAccessConversation(userId, conversationId);
        if (userIsAuthorized) {
            await addMessageToConversation(text, userId, conversationId);
            const updatedConversation = await getConversation(conversationId);
            io.emit('messagesUpdated', updatedConversation.messages);
        }
    });

    socket.on('disconnect', () => {
        console.log('Client disconnected!');
    });
});

const start = async () => {
    await db.connect('mongodb://localhost:27017');
    server.listen(8080, () => {
        console.log('Server is listening on port 8080');
    });
}

start();
