import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDvN5Jd4EZuah3nGgRkCCRhHbM1lLV1UpA",
    authDomain: "real-time-app-ebaf2.firebaseapp.com",
    projectId: "real-time-app-ebaf2",
    storageBucket: "real-time-app-ebaf2.appspot.com",
    messagingSenderId: "250970872081",
    appId: "1:250970872081:web:30e640a7f89431206694b0"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
