import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDQiOZzEeuCv4lux7b-oJ1sHPGQoBqeEWI",
    authDomain: "photo-sharing-app-92ac4.firebaseapp.com",
    projectId: "photo-sharing-app-92ac4",
    storageBucket: "photo-sharing-app-92ac4.appspot.com",
    messagingSenderId: "221829649240",
    appId: "1:221829649240:web:46b3aef743d5631e390b2d"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
