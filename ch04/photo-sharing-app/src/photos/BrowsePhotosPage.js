import { useProtectedResource } from "../data";
import { MyPhotosList } from "./myPhotosList";
import { SharedPhotosList } from "./sharedPhotosList";

export const BrowsePhotosPage = () => {
    const {
        isLoading: isLoadingMyPhotos,
        data: myPhotos,
    } = useProtectedResource('/my-photos', []);
    const {
        isLoading: isLoadingSharePhotos,
        data: sharedPhotos,
    } = useProtectedResource('/shared', []);

    return (
        <div>
            <h2 className="section-heading">My Photos</h2>
            <MyPhotosList
                isLoading={isLoadingMyPhotos}
                photos={myPhotos}/>

            <h2 className="section-heading">Shared With Me</h2>
            <SharedPhotosList
                isLoading={isLoadingSharePhotos}
                photos={sharedPhotos}/>
        </div>
    );
};
