import { useState, useEffect } from 'react';
import firebase from 'firebase/app';

export const useProtectedResource = (url, defaultValue) => {
    const [data, setData] = useState(defaultValue);

    useEffect(() => {
        const loadResource = async () => {
            const user = firebase.auth().currentUser;
            if (!user) {
                setData(defaultValue);
                return;
            }

            const response = await fetch(url, {
                headers: {
                    AuthToken: await user.getIdToken(),
                }
            });
            const jsonData = await response.json();
            setData(jsonData);
        }

        loadResource();
    }, []);

    return { data, setData };
}
