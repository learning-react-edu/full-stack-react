import * as admin from 'firebase-admin';

export const protectRouteMiddleware = async (req, res, next) => {
    try {
        const token = req.headers.authtoken;
        req.user = await admin.auth().verifyIdToken(token);
        next();
    } catch (e) {
        res.status(401).json({
            message: 'You must be logged in to access this resources'
        });
    }
};
