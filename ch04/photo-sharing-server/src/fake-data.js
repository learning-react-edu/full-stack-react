const USER_ID_1 = 'OdWk4LxT3ZSO6Cnull8SmVWrMgK2';
const USER_ID_2 = 'IfdBdDPZoafjoUdQ5KkMhJ7gc5z1';
const USER_ID_3 = '8tKFVdaOsMdM8Kse0qY7m0MPK792';

module.exports.users = [{
    id: USER_ID_1,
    email: 'brendabrown@gmail.com',
    fullName: 'Brenda Brown',
}, {
    id: USER_ID_2,
    email: 'jimsmith@gmail.com',
    fullName: 'Jim Smith',
}, {
    id: USER_ID_3,
    email: 'janejones@gmail.com',
    fullName: 'Jane Jones',
}];

module.exports.photos = [{
    url: '/cat-photo.jpeg',
    title: 'My Cat',
    ownerId: USER_ID_1,
    sharedWith: [USER_ID_2],
}, {
    url: '/car-photo.jpg',
    title: 'My Car',
    ownerId: USER_ID_2,
    sharedWith: [USER_ID_1],
}];
