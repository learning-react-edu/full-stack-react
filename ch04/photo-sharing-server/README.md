# Photo sharing server

## Generate fake data JSON files
To generate fake data JSON files enter real user IDs from firebase to the `fake-data.js` file.
Create `photo-sharing` directory in the root of the project.
Then execute following command in the root of the project:
```shell
node src/convert-to-json.js
```

This script will create 4 JSON files into `photo-sharing` directory.

## Import fake data into MongoDB

Import data from those files into mongo database with the following commands executed in `photo-sharing` directory:
```shell
mongoimport --db="photo-sharing" --collection="photos" --file="./photos.json" --jsonArray
mongoimport --db="photo-sharing" --collection="users" --file="./users.json" --jsonArray
```

### Using docker container
If MongoDB is running in docker container, then `mongoimport` should be run in the docker container shell.
First those files must be copied to the container with the following command:
```shell
docker cp "./photo-sharing" full_stack_react_mongo:"/data"
```

Here `full_stack_react_mongo` is the name of the container.

Then open docker CLI in the selected container, move to the JSON file location (`/data/photo-sharing`)
and execute same `mongoimport` command listed above.
