import { useState } from "react";
import { useUserInfo } from "../user";
import { useStockHistory } from "../stock-history";
import { StockChart } from "./StockChart";

export const DashboardPage = () => {
    const [ userInfo, setUserInfo ] = useUserInfo();
    const { cashValue, sharesValue, numberOfSharesOwned } = userInfo || {};
    const stockHistory = useStockHistory();
    const times = stockHistory.stockHistory ? Object.keys(stockHistory.stockHistory) : [];
    const prices = stockHistory.stockHistory ? Object.values(stockHistory.stockHistory) : [];
    const [ numberOfSharesValue, setNumberOfSharesValue ] = useState(0);

    const buyShares = async () => {
        const response = await fetch('/stocks/buy', {
            method: 'post',
            body: JSON.stringify({ numberOfShares: numberOfSharesValue }),
            headers: { 'Content-Type': 'application/json' },
        });
        const updateUserInfo = await response.json();
        setUserInfo(updateUserInfo);
    }

    const sellShares = async () => {
        const response = await fetch('/stocks/sell', {
            method: 'post',
            body: JSON.stringify({ numberOfShares: numberOfSharesValue }),
            headers: { 'Content-Type': 'application/json' },
        });
        const updateUserInfo = await response.json();
        setUserInfo(updateUserInfo);
    }

    return (
        <>
            <h1 className="section-heading">Stock Trading App</h1>
            <div className="centered-container">
                <StockChart
                    xValues={times}
                    yValues={prices}/>
                <div className="financial-info-section">
                    <div className="info-item">Current {stockHistory.symbol} Share Price: ${prices[prices.length - 1]}</div>
                    <div className="info-item">You currently own {numberOfSharesOwned} shares</div>
                    <div className="info-item">Cash Balance: ${cashValue}</div>
                    <div className="info-item">Portfolio Value: ${sharesValue}</div>
                    <div className="info-item">Total Value: ${cashValue + sharesValue}</div>
                    <div>
                        <input
                            type="number"
                            className="full-width space-after"
                            value={numberOfSharesValue}
                            onChange={e => setNumberOfSharesValue(e.target.value)}/>
                        <button className="buy-button" onClick={buyShares}>Buy</button>
                        <button className="sell-button" onClick={sellShares}>Sell</button>
                    </div>
                </div>
            </div>
        </>
    );
};
