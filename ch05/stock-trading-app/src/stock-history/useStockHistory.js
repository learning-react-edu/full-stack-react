import { useEffect, useState } from "react";

const zip = (keys, values) =>
    keys.reduce((acc, key, i) => ({
        ...acc,
        [key]: values[i],
    }), {});


export const useStockHistory = () => {
    const [ stockHistory, setStockHistory ] = useState([]);

    useEffect(() => {
        const loadStockHistory = async () => {
            const response = await fetch('/stock-history');
            const fullHistory = await response.json();

            const symbol = fullHistory['Meta Data']['2. Symbol'];
            const times = Object.keys(fullHistory['Time Series (30min)']);
            const prices = Object.values(fullHistory['Time Series (30min)'])
                .map(obj => obj['4. close']);

            setStockHistory({ symbol, stockHistory: zip(times, prices) });
        };

        loadStockHistory();
    }, []);

    return stockHistory;
};
