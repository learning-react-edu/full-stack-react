import { db } from "./db";

export const getUser = async () => {
    return await db.getConnection()
        .collection('users')
        .findOne({});
};
