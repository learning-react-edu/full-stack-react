import { db } from './db';

export const buyStock = async (numberOfShares, currentSharePrice) => {
    const cost = numberOfShares * currentSharePrice;

    await db.getConnection().collection('users')
        .updateOne({}, {
            $inc: {
                numberOfSharesOwned: numberOfShares,
                cashValue: -cost,
            }
        });
};
