import { getStockHistoryRoute } from "./getStockHistoryRoute";
import { getUserInfoRoute } from "./getUserInfoRoute";
import { buyStockRoute } from "./buyStockRoute";
import { sellStockRoute } from "./sellStockRoute";

export const routes = [
    buyStockRoute,
    getStockHistoryRoute,
    getUserInfoRoute,
    sellStockRoute,
];
