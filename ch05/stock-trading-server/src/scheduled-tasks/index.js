import { createRefreshStockPricesTask } from './createRefreshStockPricesTask';

const SYMBOL = 'AAPL';

export { scheduleTask } from './scheduleTask';
export const tasks = [
    createRefreshStockPricesTask(SYMBOL),
];
