import fetch from "node-fetch";

export const loadStockHistory = async (symbol) => {
    const apiUrl = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY'
    + `&symbol=${symbol}`
    + `&interval=30min`
    + `&outputsize=full`
    + `&apikey=${process.env.ALPHA_API_KEY}`;

    const response = await fetch(apiUrl);
    return await response.json();
};
